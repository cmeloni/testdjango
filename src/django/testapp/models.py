from django.db import models
from django.contrib import admin


# Create your models here.


class TipoVehiculo(models.Model):
    descripcion = models.CharField(
        max_length=150,
        verbose_name=u'Descripcion',
        null=False, blank=False)

    icono = models.CharField(
        max_length=50,
        verbose_name=u'Icono',
        null=True, blank=True)

    campo3 = models.CharField(
            max_length=50,
            verbose_name=u'Campo3',
            null=True, blank=True)

    def __unicode__(self):
        return '%d - %s' % (self.id,self.descripcion)

    def __str__(self):
        return '%d - %s' % (self.id,self.descripcion)

    class Meta:
        ordering = ['id']
        db_table = 'tipo_vehiculo'
        verbose_name_plural = "Tipo Vehículo"

    class Admin:
        pass


admin.site.register(TipoVehiculo)
